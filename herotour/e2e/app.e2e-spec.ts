import { HerotourPage } from './app.po';

describe('herotour App', () => {
  let page: HerotourPage;

  beforeEach(() => {
    page = new HerotourPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
